package com.epam;

//import org.apache.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class LifeStarts {
    public static void main(String[] args)
            throws java.lang.InterruptedException {
        Life earth = new Life(55);
        earth.drawWorld();
        while (true) {
            Thread.sleep(200);
            earth.nextGeneration();
            earth.drawWorld();
        }
    }

}
